# Coding-Portfolio 23.08.23 | Hendrik

---

| **Ziele**                   | **Gelernte Befehle**   | **Unklare Punkte**    | **Nächste Ziele**    | **Repository-Link**    |
|----------------------------|------------------------|-----------------------|----------------------|------------------------|
| 🚀 Im Block 2 vorankommen  | 💻 -       | ❓ Ich verstehe die Diagramme noch nicht gut      | 🎯 Der beste in diesen Thema zu sein     | 📦 https://gitlab.com/ksb5971534/M319       |

---
