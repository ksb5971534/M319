# Main Portfolio

## Block 1

| **Was ist mein Ziel für heute?** |                             |
|----------------------------------|-----------------------------|
| **Was habe ich gelernt?**        | - Markdown verwenden und PDF-Erstellung |
|                                  |                             |
| **Was bereitet mir noch Schwierigkeiten?** | - Anfangs schwierig, schnell und effizient zu arbeiten |
|                                          | - Alles ist noch neu, aber ich bin zuversichtlich, dass ich mich daran gewöhnen werde |
|                                          |                             |
| **Wie löse ich diese Schwierigkeiten?**   | - Durchhalten und im Laufe der Zeit verbessern |
|                                          | - Wenn ich nicht weiterkomme, frage ich meinen Vater |
